import datetime

from five import grok
from zope import schema
from zope.interface import implements
from Products.Five.browser import BrowserView
from Products.CMFCore.utils import getToolByName

from zope.schema.interfaces import IVocabularyFactory
from zope.schema.vocabulary import SimpleVocabulary

from plone.directives import form, dexterity
from plone.dexterity.content import Container

from tabellio.agenda.interfaces import MessageFactory as _
import tabellio.config.utils

import utils

class AgendaModeVocabulary(object):
    grok.implements(IVocabularyFactory)

    def __call__(self, context):
        values = [('month', _(u'Month')), ('list', _(u'List')), ('seances', _(u'Seances'))]
        terms = []
        for key, value in values:
            terms.append(SimpleVocabulary.createTerm(key, key, value))
        return SimpleVocabulary(terms)
grok.global_utility(AgendaModeVocabulary, name=u'tabellio.agenda.agendaModes')

class AgendaSelectorVocabulary(object):
    grok.implements(IVocabularyFactory)

    def __call__(self, context):
        values = [('all', _(u'All events')),
                  ('parlevent', _(u'Parlementary Events')),
                  ('burevent', _(u'Bureau Events')),
                  ('comevent', _(u'Commission Events')),
                  ('parlcomevent', _(u'Parlmentary, Commission and Bureau Events')),
                  ('genevent', _(u'General Events'))]
        terms = []
        for key, value in values:
            terms.append(SimpleVocabulary.createTerm(key, key, value))
        return SimpleVocabulary(terms)
grok.global_utility(AgendaSelectorVocabulary, name=u'tabellio.agenda.agendaSelector')


class IAgendaFolder(form.Schema):
    title = schema.TextLine(title=_(u'Title'))
    mode = schema.Choice(title=_(u'Mode'),
            vocabulary='tabellio.agenda.agendaModes')
    selector = schema.Choice(title=_(u'Event Selector'), default=u'all',
            vocabulary='tabellio.agenda.agendaSelector')
    view_switcher = schema.Bool(title=_(u'Show view switcher'), default=False)


class AgendaFolder(Container):
    implements(IAgendaFolder)


class View(grok.View, utils.MonthlyView):
    grok.context(IAgendaFolder)
    grok.require('zope2.View')
    viewname = ''

    def has_view_switcher(self):
        return self.context.view_switcher == True

    def as_list(self):
        return self.context.mode == 'list'

    def as_month(self):
        return self.context.mode == 'month'

    def as_seances(self):
        return self.context.mode == 'seances'

    def update(self):
        utils.MonthlyView.update(self)
        return self.updated

    def getYearAndMonthToDisplay(self):
        year = self.request.form.get('navyear') or self.now[0]
        month = self.request.form.get('navmonth') or self.now[1]
        year, month = int(year), int(month)
        return year, month

    def shown_events(self):
        if self.context.selector == 'parlevent':
            return ['parl']
        elif self.context.selector == 'comevent':
            return ['com']
        elif self.context.selector == 'burevent':
            return ['bur']
        elif self.context.selector == 'genevent':
            return ['gen']
        elif self.context.selector == 'parlcomevent':
            return ['parl', 'com', 'bur']
        else:
            return ['parl', 'com', 'bur', 'gen']

    def get_events_from_catalog(self, start, end, **keywords):
        if self.context.selector == 'parlevent':
            portal_type = ['tabellio.agenda.parlevent']
        elif self.context.selector == 'comevent':
            portal_type = ['tabellio.agenda.comevent']
        elif self.context.selector == 'burevent':
            portal_type = ['tabellio.agenda.burevent']
        elif self.context.selector == 'genevent':
            portal_type = ['tabellio.agenda.event']
        elif self.context.selector == 'parlcomevent':
            portal_type = ['tabellio.agenda.parlevent',
                           'tabellio.agenda.comevent',
                           'tabellio.agenda.burevent',
                          ]
        else:
            portal_type = ['tabellio.agenda.event',
                           'tabellio.agenda.parlevent',
                           'tabellio.agenda.burevent',
                           'tabellio.agenda.comevent',
                          ]

        return utils.MonthlyView.get_events_from_catalog(self, start, end,
                portal_type=portal_type, **keywords)

    def getMonthEvents(self):
        year, month = self.getYearAndMonthToDisplay()
        last_day = self.calendar._getCalendar().monthrange(year, month)[1]
        first_date = self.calendar.getBeginAndEndTimes(1, month, year)[0]
        last_date = self.calendar.getBeginAndEndTimes(last_day, month, year)[1]

        brains = self.get_events_from_catalog(last_date, first_date, sort_on='start')
        return [x.getObject() for x in brains]

    def getMonthEventsWithRepetition(self):
        events = self.getMonthEvents()
        year, month = self.getYearAndMonthToDisplay()
        for event in events[:]:
            if not event.multidays():
                continue
            if event.start.month != month:
                events.remove(event)
            repeated = event.end.toordinal()-event.start.toordinal()
            for i in range(repeated):
                if (event.start+datetime.timedelta(i+1)).month != month:
                    continue
                # duplicate event with minimal infos
                new_event = event.__class__()
                new_event.id = event.id
                new_event.title = event.title
                new_event.place = event.place
                new_event.text = event.text
                new_event.description = event.description
                new_event.start = event.start+datetime.timedelta(i+1)
                new_event.end = event.end
                # it's kind of ugly but this is definitely the easiest way to
                # get the item pointing to the right URL
                new_event.absolute_url = event.absolute_url()
                events.append(new_event)
        events.sort(lambda x,y: cmp(x.start, y.start))
        return events

    def todayanchor(self):
        return 'd%d-%d-%d' % datetime.datetime.today().timetuple()[:3]

class List(View):
    grok.context(IAgendaFolder)
    grok.require('zope2.View')
    grok.template('view')
    viewname = 'list'

    def as_list(self):
        return True

    def as_month(self):
        return False

    def as_seances(self):
        return False
