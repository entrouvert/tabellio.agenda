
from StringIO import StringIO

from plone.memoize import ram
from plone.memoize.compress import xhtml_compress
from plone.portlets.interfaces import IPortletDataProvider

from zope import component

from zope.interface import implements
from zope.component import getMultiAdapter

from Acquisition import aq_inner
from DateTime import DateTime
from Products.CMFCore.utils import getToolByName
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from Products.PythonScripts.standard import url_quote_plus

from plone.registry.interfaces import IRegistry

from plone.app.portlets import PloneMessageFactory as _
from plone.app.portlets import cache
from plone.app.portlets.portlets import base

from tabellio.config.interfaces import ITabellioSettings

from utils import MonthlyView


class IAgendaPortlet(IPortletDataProvider):
    """A portlet displaying an agenda
    """


class AgendaPortletAssignment(base.Assignment):
    implements(IAgendaPortlet)

    title = _(u'Agenda')


def _render_cachekey(fun, self):
    context = aq_inner(self.context)
    if not self.updated:
        self.update()

    if self.calendar.getUseSession():
        raise ram.DontCache()
    else:
        portal_state = getMultiAdapter((context, self.request), name=u'plone_portal_state')
        key = StringIO()
        print >> key, portal_state.navigation_root_url()
        print >> key, cache.get_language(context, self.request)
        print >> key, self.calendar.getFirstWeekDay()

        year, month = self.getYearAndMonthToDisplay()
        print >> key, year
        print >> key, month

        start = DateTime('%s/%s/1' % (year, month))
        end = DateTime('%s/%s/1 23:59:59' % self.getNextMonth(year, month)) - 1

        def add(brain):
            key.write(brain.getPath())
            key.write('\n')
            key.write(brain.modified)
            key.write('\n\n')

        brains = self.get_events_from_catalog(start, end)
        for brain in brains:
            add(brain)

        return key.getvalue()


class AgendaPortletRenderer(base.Renderer, MonthlyView):
    _template = ViewPageTemplateFile('portlet_agenda.pt')

    def __init__(self, context, request, view, manager, data):
        base.Renderer.__init__(self, context, request, view, manager, data)
        MonthlyView.__init__(self, context)
        self.url_quote_plus = url_quote_plus

    def update(self):
        base.Renderer.update(self)
        MonthlyView.update(self)


    @ram.cache(_render_cachekey)
    def render(self):
        return xhtml_compress(self._template())

    def getYearAndMonthToDisplay(self):
        session = None
        request = self.request

        # First priority goes to the data in the REQUEST
        year = request.get('year', None)
        month = request.get('month', None)

        # Next get the data from the SESSION
        if self.calendar.getUseSession():
            session = request.get('SESSION', None)
            if session:
                if not year:
                    year = session.get('calendar_year', None)
                if not month:
                    month = session.get('calendar_month', None)

        # Last resort to today
        if not year:
            year = self.now[0]
        if not month:
            month = self.now[1]

        year, month = int(year), int(month)

        # Store the results in the session for next time
        if session:
            session.set('calendar_year', year)
            session.set('calendar_month', month)

        # Finally return the results
        return year, month


    def getReviewStateString(self):
        states = self.calendar.getCalendarStates()
        return ''.join(map(lambda x: 'review_state=%s&amp;' % self.url_quote_plus(x), states))

    def getQueryString(self):
        request = self.request
        query_string = request.get('orig_query',
                                   request.get('QUERY_STRING', None))
        if len(query_string) == 0:
            query_string = ''
        else:
            query_string = '%s&amp;' % query_string
        return query_string

    def agenda_url(self):
        settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
        path = settings.generalagendaPath
        portal_state = getMultiAdapter((self.context, self.request), name=u'plone_portal_state')
        return portal_state.navigation_root_url() + '/' + path


class AgendaPortletAddForm(base.NullAddForm):

    def create(self):
        return AgendaPortletAssignment()
