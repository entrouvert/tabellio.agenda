from zope import schema
from zope.interface import implements
from z3c.relationfield.schema import RelationChoice

from plone.formwidget.contenttree import ObjPathSourceBinder

from themis.datatypes.commission import ICommission

from tabellio.agenda.interfaces import MessageFactory as _
from parlevent import IParlEvent, ParlEvent


class IComEvent(IParlEvent):
    commission = RelationChoice(
                title=_(u"Commission"),
                source=ObjPathSourceBinder(object_provides=ICommission.__identifier__),
                required=False)


class ComEvent(ParlEvent):
    implements(IComEvent)

    def klass(self):
        return 'commission-event'

    @property
    def reftitle(self):
        return self.title
