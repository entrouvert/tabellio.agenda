# -*- coding: utf-8 -*-

import datetime

from zope import schema
from zope.interface import implements
from Products.Five.browser import BrowserView
from Products.CMFCore.utils import getToolByName
from Acquisition import aq_parent

from zope.i18n.locales import locales
from plone.directives import form, dexterity
from plone.dexterity.content import Item
from plone.app.textfield import RichText

from tabellio.agenda.interfaces import MessageFactory as _

from utils import MonthlyView


class IBaseEvent(form.Schema):
    title = schema.TextLine(title=_(u'Title'))
    description = schema.Text(title=_(u'Description'), required=False)

    place = schema.TextLine(title=_(u'Place'))

    start = schema.Datetime(title=_(u'Start'))
    end = schema.Datetime(title=_(u'End'), required=False)

    text = RichText(title=_(u'Text'), required=False)


class IEvent(IBaseEvent):
    pass



class BaseEvent(Item):
    def longdatetime(self):
        # unfortunately this forces the locale
        formatter = locales.getLocale('fr').dates.getFormatter('dateTime', 'medium')
        if self.start.hour == 0 and self.start.minute == 0:
            formatter.setPattern(u'EEEE d MMMM yyyy')
        else:
            formatter.setPattern(u'EEEE d MMMM yyyy à HH:mm')
        return formatter.format(self.start)
    longdatetime = property(longdatetime)

    def shortdatetime(self):
        # unfortunately this forces the locale
        formatter = locales.getLocale('fr').dates.getFormatter('dateTime', 'medium')
        formatter.setPattern(u'd MMMM yyyy')
        return formatter.format(self.start)
    shortdatetime = property(shortdatetime)

    def longenddate(self):
        # unfortunately this forces the locale
        formatter = locales.getLocale('fr').dates.getFormatter('dateTime', 'medium')
        formatter.setPattern(u'EEEE d MMMM yyyy')
        return formatter.format(self.end)
    longenddate = property(longenddate)

    def multidays(self):
        if not self.end:
            return False
        return (self.start.timetuple()[:3] != self.end.timetuple()[:3])

    def start_month(self):
        formatter = locales.getLocale('fr').dates.getFormatter('dateTime', 'medium')
        formatter.setPattern(u'MMMM')
        return formatter.format(self.start)

    def start_hour(self):
        if self.start.hour == 0 and self.start.minute == 0:
            return ''
        return '%dh%02d' % (self.start.hour, self.start.minute)

    def end_hour(self):
        return '%dh%02d' % (self.end.hour, self.end.minute)

    def agenda_url(self):
        parent = aq_parent(self)
        if parent.portal_type == 'tabellio.agenda.folder':
            return parent.absolute_url()
        catalog = getToolByName(self, 'portal_catalog')
        try:
            return catalog(portal_type='tabellio.agenda.folder')[0].getURL()
        except IndexError:
            return '#'

    def is_now(self):
        now = datetime.datetime.now()
        if now < self.start:
            return False
        if self.end and now > self.end:
            return False
        return True

    def todayclass(self):
        if self.start.timetuple()[:3] == datetime.datetime.today().timetuple()[:3]:
            return 'today'
        return ''

    def preview(self):
        n = 3

        def nth_position(string, sub, n, pos = 0):
            if n:
                sliced_from = string.find(sub) + len(sub)
                return pos + nth_position(string[sliced_from:],
                                          sub, n-1, sliced_from)
            return pos
        return self.text.output[:nth_position(self.text.output, '</p>', n)]

class Event(BaseEvent):
    implements(IEvent)

    def klass(self):
        return 'generic-event'

class EventBaseView:
    def next_event_url(self, portal_type=None):
        # do not go more than 100 days in the future
        end = self.context.start + datetime.timedelta(100)
        start = self.context.start

        if portal_type is None:
            portal_type = ('tabellio.agenda.parlevent',
                           'tabellio.agenda.comevent',
                           'tabellio.agenda.burevent',
                           )

        monthly_view = MonthlyView(self.context, init_calendar=True)
        serie_of_events = monthly_view.get_events_from_catalog(
                start, end, portal_type=portal_type)
        for i, event in enumerate(serie_of_events):
            event_id = ((type(event.getId) is str) and event.getId or event.getId())
            if event_id == self.context.id:
                try:
                    return serie_of_events[i+1].getURL()
                except IndexError:
                    return None
        return None

    def previous_event_url(self, portal_type=None):
        end = self.context.start
        # do not go back more than 100 days in the past
        start = self.context.start - datetime.timedelta(100)

        if portal_type is None:
            portal_type = ('tabellio.agenda.event',
                           'tabellio.agenda.parlevent',
                           'tabellio.agenda.comevent',
                           'tabellio.agenda.burevent'
                           )

        monthly_view = MonthlyView(self.context, init_calendar=True)
        serie_of_events = monthly_view.get_events_from_catalog(
                start, end, portal_type=portal_type)
        for i, event in enumerate(serie_of_events):
            event_id = ((type(event.getId) is str) and event.getId or event.getId())
            if event_id == self.context.id:
                if i == 0:
                    return None
                return serie_of_events[i-1].getURL()
        return None

