import os

from zope import schema, component
from zope.interface import implements
from z3c.relationfield.schema import RelationChoice
from Products.CMFCore.utils import getToolByName

from plone.formwidget.contenttree import ObjPathSourceBinder

from themis.fields import LegisSession
from plone.app.textfield import RichText

from plone.registry.interfaces import IRegistry
from tabellio.config.interfaces import ITabellioSettings

from tabellio.agenda.interfaces import MessageFactory as _
from event import IBaseEvent, BaseEvent, EventBaseView


class IParlEvent(IBaseEvent):
    session = LegisSession(title=_(u'Session'))
    order_of_the_day = RichText(title=_(u'Order of the day'), required=False)

    cri = RelationChoice(title=_(u'Minutes'), required=False,
                                    source=ObjPathSourceBinder())


class ParlEvent(BaseEvent):
    implements(IParlEvent)

    def klass(self):
        return 'parlementary-event'

    def get_stream_url(self):
        # this brings some knowledge about the view into the object, this is
        # not ideal.
        settings = component.getUtility(IRegistry).forInterface(ITabellioSettings, False)
        if settings.audiofiles_path:
            if os.path.exists(os.path.join(settings.audiofiles_path, self.get_stream_name())):
                if settings.embedded_audio_player_url:
                    return settings.embedded_audio_player_url + '?' + self.get_stream_name()
                else:
                    return self.absolute_url() + '/ecouter.m3u'
        if not self.is_now():
            return None

        if settings.embedded_audio_player_url:
            return settings.embedded_audio_player_url
        else:
            return self.absolute_url() + '/ecouter.m3u'

    def get_stream_name(self):
        filename = '%04d%02d%02d%02d-SEAN.mp3' % (
                        self.start.year, self.start.month,
                        self.start.day, self.start.hour)
        return filename

class ParlEventBaseView(EventBaseView):
    def next_event_url(self):
        return EventBaseView.next_event_url(self,
                portal_type=('tabellio.agenda.parlevent',
                             'tabellio.agenda.burevent',
                             'tabellio.agenda.comevent'))

    def previous_event_url(self):
        return EventBaseView.previous_event_url(self,
                portal_type=('tabellio.agenda.parlevent',
                             'tabellio.agenda.burevent',
                             'tabellio.agenda.comevent'))
