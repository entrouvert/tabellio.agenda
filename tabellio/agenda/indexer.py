from zope.interface import Interface
from five import grok
from plone.indexer import indexer
from plone.z3cform import z2
from z3c.form.interfaces import IFormLayer

from event import IEvent
from parlevent import IParlEvent
from comevent import IComEvent
from burevent import IBurEvent

class IEventIndexer(Interface):
    """Dexterity behavior interface for enabling the dynamic SearchableText
       indexer on Event objects."""


class FakeView(object):
    """This fake view is used for enabled z3c forms z2 mode on.
    """

    def __init__(self, context, request):
        self.context = context
        self.request = request


@indexer(IEventIndexer)
def event_dynamic_searchable_text_indexer(obj):
    """Dynamic searchable text indexer.
    """

    # We need to make sure that we have z2 mode switched on for z3c form.
    # Since we do not really have any view to do this on, we just use
    # a fake view. For switching z2 mode on, it's only necessary that
    # there is a view.request.
    view = FakeView(obj, obj.REQUEST)
    z2.switch_on(view, request_layer=IFormLayer)

    indexed = []

    for fieldname in ('title', 'place',):
        try:
            value = getattr(obj, fieldname)
        except AttributeError:
            continue

        if not value:
            continue

        if type(value) is list:
            for item in value:
                item_value = item.to_object.title
                # be sure that it is utf-8 encoded
                if isinstance(item_value, unicode):
                    item_value = item_value.encode('utf-8')
                indexed.append(item_value)
        else:
            if isinstance(value, unicode):
                value = value.encode('utf-8')
            indexed.append(value)

    return ' '.join(indexed)


grok.global_adapter(event_dynamic_searchable_text_indexer,
                    name='SearchableText')
