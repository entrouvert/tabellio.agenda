from zope import schema
from zope.interface import implements
from z3c.relationfield.schema import RelationChoice

from plone.formwidget.contenttree import ObjPathSourceBinder

from tabellio.agenda.interfaces import MessageFactory as _
from parlevent import IParlEvent, ParlEvent


class IBurEvent(IParlEvent):
    pass


class BurEvent(ParlEvent):
    implements(IBurEvent)

    def klass(self):
        return 'bureau-event'

